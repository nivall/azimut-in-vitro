## SOCS1 analyses and figures
#' Project: AZIMUT
#' Author: Nicolas Vallet

## Require
library(ggplot2)
library(tidyverse)
library(RColorBrewer)
library(reshape2)
library(ggrepel)

## Create a theme
custom_theme <- list(theme_minimal(),
                     theme(axis.title.x = element_blank(),
                           axis.text    = element_text(color = "black"),
                           axis.ticks   = element_line(color = "black"),
                           axis.line    = element_line(color = "black"),
                           panel.grid.major = element_blank(),
                           panel.grid.minor = element_blank()
                           ),
                     ylab("Relative MFI (SOC1 / isotype control)"),
                     scale_fill_manual(values = c( "#bcbddc",  "#807dba" ))
                     )  

## Import data
setwd("~/Git/azimut-in-vitro/facs_socs1")
input <- read.csv("socs1.csv", sep = ";", dec = ".") %>%
    separate(sample, into = c("sample", "treatment", "iso"), remove = TRUE) %>%
    add_column(group = paste(.$sample, .$treatment, sep = "_"))

## Normalize to isotype control
norm_input <- as.data.frame(input)      
for(i in 1:nrow(norm_input) ) {
    ## get line with isotype
    ref_line <- which( norm_input[, "group"] == norm_input[i, "group"] & norm_input[, "iso"] == "ISO")
    ## normalize only SOCS samples
    if(norm_input[i, "iso"] != "ISO") {
        ## perform normalization for cd4socs
        norm_input[i, "cd4socs"] <- norm_input[i, "cd4socs"] / norm_input[ref_line, "cd4socs"]
        ## perform normalization for cd8socs
        norm_input[i, "cd8socs"] <- norm_input[i, "cd8socs"] / norm_input[ref_line, "cd8socs"]
    }   
}
norm_input <- norm_input %>% filter(iso == "SOCS") %>%
    select(-c(iso, group))

## Get the result plot
plot <- norm_input %>%
    melt(id.vars = c("sample", "treatment")) %>%
    mutate(treatment = fct_relevel(treatment, "DMSO", "AZM10")) %>%
    ggplot(aes( x = treatment, y = value, fill = treatment) ) +
    facet_wrap(. ~ variable) +
    geom_boxplot(color = "black") +
    geom_line(aes(group = sample), color = "black", linetype = "dotted") +
    geom_point(shape = 21, color = "black", fill = "white", size = 2) +
    custom_theme

## Get the statistical test
testcd8 <- norm_input %>%
    wilcox_test(cd8socs ~ treatment, paired = TRUE, p.adjust.method = "fdr")
testcd4 <- norm_input %>%
    wilcox_test(cd4socs ~ treatment, paired = TRUE, p.adjust.method = "fdr")

## Print SOCS1 plot
setwd("~/tmp/")
pdf("norm_rfi.pdf", width = 4, height = 5)
print(plot)
dev.off()

## Print the statistical test result
sink(file = "socs_stats.txt")
cat("CD8 ------------ \n\n")
print(testcd8)
cat("\n\n\nCD4 ----------- \n\n")
print(testcd4)
sink()
