# GUIX configuration

This folder contains all files necessary to generate the environment from GUIX
See : https://hpc.guix.info/ and https://guix.gnu.org/

* channels.scm : gets the commit number to get reproductible versions of the environment
* manifest.scm : gets the packages stored in GUIX

Run either the `runguix.sh` file or use the commande line below to load the environment
```
guix time-machine -C channels.scm -- environment -m manifest.scm -- R
```
