;; This file describe software and libraries used in this repository
(specifications->manifest
 (list
  "r"
  "r-ggplot2"
  "r-reshape2"
  "r-rcolorbrewer"
  "r-tidyverse"
  "r-rstatix"
  "r-cowplot"
  "r-ggrepel"
  "r-scales"
  ))
