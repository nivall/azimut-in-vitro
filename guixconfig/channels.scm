;; This file point to the Guix channel.
(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
	(branch "master")
	(commit "74a8f79f4d1b8d32822952eafea1683bb0b6dba0")))
(list (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (branch "master")
        (commit
          "b5d472ef5d5c5e36126d5d949dc0b8d7c7610142")
        (introduction
          (make-channel-introduction
            "9edb3f66fd807b096b48283debdcddccfea34bad"
            (openpgp-fingerprint
              "BBB0 2DDF 2CEA F6A8 0D1D  E643 A2A0 6DF2 A33A 54FA")))))
