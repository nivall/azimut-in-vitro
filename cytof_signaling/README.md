# Signaling pathways analysis following CD3/CD28 activation in azithromycin-treated or untreated PBMC

This directory stores scripts and data to reproduce analyses of debarcoded `FCS` files. These files are available on [FlowRepository](https://flowrepository.org) experiment ID [FR-FCM-Z5L7](https://flowrepository.org/id/FR-FCM-Z5L7).

## Metadata

* Metadata of `FCS` files (`md_8HD.xlsx`)
* Metaclusters identification after manual check of antigen expression (`meta30.xlsx`)
* Panel informations to build `SCE` object (`panel.xlsx`)

## Processing pipeline (`scripts`)

* Prepare the `SCE` object from `FCS` files (`PrepDataSce.R`)
* Perform FlowSOM clustering and extract data from `SCE` object (`FlowsomMerge.R`)
* Normalize extracted data to T0 values (`SceNormalization.R`)
* Script to load ordered lists used for visualization (`Variable.R`)

## Processed data

* Raw data of mean expression extracted from SCE (`sce_data8HD.csv`)
* Normalized mean expression to T0 (`normalizedT0_data.csv`)

## Figures

* Draw dotplots of differentially detected signaling proteins between azithromycin and DMSO conditions (`AUC_T0.R`)
* Draw heatmap of mean intensities according to time (`HeatmapImmunePop.R`)
* Draw heatmap of difference in intensities according to time between AZM and DMSO (`HeatmapRatio.R`)
