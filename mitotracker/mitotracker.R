## Mitotracker experiment
#' Project: AZIMUT
#' Author: Nicolas Vallet

## Require
library(ggplot2)
library(RColorBrewer)
library(tidyverse)
library(reshape2)
library(rstatix)

## Import result table
setwd("~/Git/azimut-in-vitro/mitotracker")
input <- read.csv2("mitotracker.csv") %>%
    separate(1, into = c("sample", "treatment"))

## Compute relative changes to control
rel_input <- input
for(i in 1:nrow(rel_input) ) {
    ## get line with referent
    ref_line <- which(input$sample == rel_input[i, "sample"] & input$treatment == "DMSO")
    for(j in c("cd4mean", "cd8mean") ) {
        rel_input[i, j] <- rel_input[i, j] / input[ref_line, j]
    }
}

## Draw the result plot
plot <- input %>%
    melt(id.vars = c("sample", "treatment")) %>%
    mutate(treatment = fct_relevel(treatment, "Unstim", "DMSO", "A10") ) %>%
    ggplot(aes(x = treatment, y = value, fill = treatment) ) +
    facet_wrap(. ~ variable) +
    geom_boxplot() +
    geom_line(aes(group = sample), linetype = "dotted")+
    geom_point(shape = 21, fill = "white", color = "black") +
    theme_minimal()+
    scale_fill_manual(values = c( "darkgrey", "#bcbddc",  "#807dba" )) +
    ylab("Mitotracker MFI") +
    theme(axis.title.x = element_blank(),
                           axis.text    = element_text(color = "black"),
                           axis.ticks   = element_line(color = "black"),
                           axis.line    = element_line(color = "black"),
                           panel.grid.major = element_blank(),
                           panel.grid.minor = element_blank()
                           )

## Compute statistical tests
testcd4 <- input %>%
    wilcox_test(cd4mean ~ treatment, paired = TRUE, p.adjust.method = "fdr")
testcd8 <- input %>%
    wilcox_test(cd8mean ~ treatment, paired = TRUE, p.adjust.method = "fdr")

## Print result
setwd("~/tmp")
pdf("mitotrackerpool.pdf", height = 4, width =  4)
print(plot)
dev.off()

## Print statistical results
sink(file = "mitotrackstat.txt")
cat("\n\n")
cat("\n\n\n CD8 -------- \n\n")
print(testcd8)
cat("\n\n\n CD4 -------- \n\n")
print(testcd4)
sink()
