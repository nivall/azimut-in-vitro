# Deciphering impact of azithromycin on cultured T cells.

# Purpose
Relapse after allogeneic hematopoietic stem cell transplantation remains the first cause of death and understanding its mechanisms is an unmet clinical need. Azithromycin evaluated to prevent pulmonary graft-versus-host disease, was associated with relapse in a previous phase 3 randomized placebo-controlled trial. 

The aim of this repository is to store results from in vitro experiments and the corresponding source code to draw plots and compute analyses.

To be sure to reproduce easily the analyses make sure to clone in repo in `~/Git/` directory and to create the `~/tmp` directory for the outputs.

# Computing environment (`guixconfig`)
To reproduce analyses, scripts can be ran in a [GNU Guix environment](https://guix.gnu.org/en/about/) with the `channels.scm` and `manifest.scm` files. Use the command below to run `R`.

```
guix time-machine -C channels.scm -- environment -m manifest.scm -- R
```

Additionnal informations on Guix environment can be found in the following ressources:

- [Our git repository](https://gitlab.com/nivall/guixreprodsci)
- [Guix HPC website](https://hpc.guix.info/)

# Repository contents

## CAR-T cytotoxicity (`cart_cytotoxicity`)
Results from anti-CD19 CAR-T cells cocultured with NALM6 (CD19 positive, FFLuc-GFP) cells. Results shown are the percent killing, calculated based upon the average loss of luminescence relative to the control wells containing only tumor cells. Ratio of CAR-T cells:NALM6 cells are described as variables.

## Cell viability (`cell_viability`)
Results from trypan blue exclusion assays. CAR-T cells cultured for 24h in with azithromycin and T cells 48h after CD3/CD28 activation. Viability assessed by flow cytometry is described in `FACS proliferation` directory.

## Class II HLA expression on CD3 negative cells sorted from PBMC (`class2hlapan_cd3neg`)
Results of flow cytometry assessment of expression of pan class II HLA expression on CD3 negatives cells from thawed PBMC from healthy donors. Cells were incubated for 24h with azithromycin or control.

## Signaling pathways analysis with mass cytometry (`cytof_signaling`)
Pipeline, data and scripts to analyse data from mass cytometry experiments. Source `FCS` files are stored in the [FlowRespository](https://flowrepository.org) experiment ID FR-FCM-Z5L7.

## Cytokines (`cytokines`)
Results from cytokines multiplex assay on cell culture supernatants two days (D2) and five days (D5) following CD3/CD28 activation. The script `cytokines.R` draw individual plots with p-values while `cytokines_summary_fig.R` draws the summary figure.

## FACS proliferation (`facs_proliferation`)
Results from flow cytometry experiments 48h after CD3/CD28 activation. Proliferation was studied with CFSE staining. Percentage of living cells was evaluated by viability staining.

## FACS SOCS1 (`facs_socs1`)
SOCS1 expression was assessed by flow cytometry experiment six days after CD3/CD28 activation. Analyses compare ratio of SOCS1/Isotype control. Variables represent mean fluorescence intensity in CD4 and CD8 subsets

## Mitotracker (`mitotracker`)
Mitotracker staining 48h after CD3/CD28 activation. Variables represent mean fluorescence intensity in CD4 and CD8 subsets.

## Proliferation assays with ciclosporin A (`prolif_CsA`)
Results from flow cytometry experiments 48h after CD3/CD28 activation. Here co-treatment with ciclosporin A was assessed.

## Seahorse glycolysis assays following activation (`seahorse_glycolysis`)
CD4+ and CD8+ sorted cells from PBMC of healthy donor were activated in vitro with CD3/CD28 antibody complexes (Agilent Seahorse T cell activation kit). This directory contains the scripts and raw data to reproduce analysis and figures from the manuscript.

## Seahorse mitochondria (OCR) stress test after 5 days of treatment in vitro (`seahorse_ocrD5`)
This directory contains the data and scripts to reproduce the analyses from mitochondria stress test (Agilent Kit) performed on CD3+ sorted cells from healthy donors treated with azithromycin for 5 days.

## TMRM assays (mitochondria membrane potential assay) (`tmrm`)
This directory contains data and scripts to reproduce the analyses from TMRM assays 48h after T cells activation in vitro. 

## Tumor cell lines (`tumor_cell_lines`)
Results from tumor cell lines cultures with azithromycin. Cell expansion was measured after two days (D2) and five days (D5) of culture.

## Tumor primary cells (`tumor_primarycells`)
Results from patients primary blasts cells incubated with azithromycin. Cell expansion and HLA II expression was assessed after 3 days of culture.

## Azithromycin wash out experiments (`washout`)
This directory contains data and scripts to reproduce the analyses from azithromycin wash out assays.
