## Cytokines analyses - draw individual plots and get pvalues
#' Project: AZIMUT
#' Author: Nicolas Vallet

## Require
library(ggplot2)
library(tidyverse)
library(reshape2)
library(RColorBrewer)
library(rstatix)

## Save a theme for the outputs
custom_theme = list(
    theme_minimal(),
    theme(axis.title = element_text(color = "black", size = 15),
          axis.title.x = element_blank(),
          axis.text = element_text(color = "black", size = 15),
          axis.line = element_line(color = "black"),
          panel.grid = element_blank(),
          plot.title = element_text(size = 8),
          axis.ticks = element_line(color = "black")),
    scale_fill_manual(values = c( "#bcbddc",  "#807dba","#3f007d"))
    )

## Import data
cytok = read.csv2("~/Git/azimut-in-vitro/cytokines/cytokines.csv",
                check.names = FALSE) %>%
    select(- c("type", "well") ) %>%
    separate(sample, into = c("donor", "timing", "treatment") ) %>%
    mutate(treatment = fct_relevel(treatment, "US", "DMSO", "A10", "A20")) 

## Replace min and max by 90% min and 110% max, respectively
for(i in names(cytok)[4:28]) {
    min_i = min(as.numeric(cytok[, i]), na.rm = TRUE)
    max_i = max(as.numeric(cytok[, i]), na.rm = TRUE)
    cytok[, i] = ifelse(cytok[, i] == "OOR<",
                   0.9*min_i,
                   cytok[, i])
    cytok[, i] = ifelse(cytok[, i] == "OOR >",
                        1.1*max_i,
                        cytok[, i])
    cytok[, i] = as.numeric(cytok[, i])
}

## Loop to draw figures D2
time = "D2"
list_plot_d2 = list()
cytok_loop = cytok %>% filter(timing == time)
iteration = 1

for(i in 4:28) {

    cytokine = names(cytok_loop)[i]

    val_dmso = cytok_loop %>% filter(treatment == "DMSO") %>% pull(all_of(i))
    val_a10  = cytok_loop %>% filter(treatment == "A10" ) %>% pull(all_of(i))
    val_a20  = cytok_loop %>% filter(treatment == "A20" ) %>% pull(all_of(i))

    testa10 = wilcox.test(val_dmso, val_a10, paired = TRUE, correct = FALSE)
    testa20 = wilcox.test(val_dmso, val_a20, paired = TRUE, correct = FALSE)
    
    pvala10 = paste("DMSO v A10=", testa10$p.value, sep = "")
    pvala20 = paste("DMSO v A20=", testa20$p.value, sep = "")

    title = paste("D2", cytokine, pvala10, pvala20, sep = " -- ")

    ylabel = paste(cytokine, "pg/mL", sep = " ")

    plot_i = cytok_loop %>%
        select(donor, treatment, all_of(i)) %>%
        melt(id.vars = c("donor", "treatment")) %>%
        ggplot(aes(x = treatment, y = value, fill = treatment)) +
        geom_boxplot(color = "black")+
        geom_line(aes(group = donor), linetype = "dotted", color = "black") +
        geom_point(shape = 21, fill = "white", color = "black", size = 2)+
        ggtitle(title) +
        ylab(ylabel)+
        custom_theme

    list_plot_d2[[iteration]] = plot_i
    
    iteration = iteration + 1 
        
    }
    
pdf("~/tmp/cytokines_culture_d2.pdf", width = 5, height= 5)
for(i in 1:length(list_plot_d2)) {
print(list_plot_d2[[i]])
}
dev.off()


## Loop to draw figures D5
time = "D5"
list_plot_d5 = list()
cytok_loop = cytok %>% filter(timing == time) %>%
    mutate(donor = as.factor(.$donor)) %>%
    arrange(donor)
           
iteration = 1

for(i in 4:28) {

    cytokine = names(cytok_loop)[i]

    val_dmso = cytok_loop %>% filter(treatment == "DMSO") %>% group_by(donor) %>% pull(all_of(i))
    val_a10  = cytok_loop %>% filter(treatment == "A10" ) %>% group_by(donor) %>% pull(all_of(i))
    val_a20  = cytok_loop %>% group_by(donor, treatment) %>% filter(treatment == "A20" ) %>% pull(all_of(i))

    testa10 = wilcox.test(val_dmso, val_a10, paired = TRUE, correct = FALSE)
    testa20 = wilcox.test(val_dmso, val_a20, paired = TRUE, correct = FALSE)
    
    pvala10 = paste("DMSO v A10=", testa10$p.value, sep = "")
    pvala20 = paste("DMSO v A20=", testa20$p.value, sep = "")

    title = paste("D5", cytokine, pvala10, pvala20, sep = " -- ")

    ylabel = paste(cytokine, "pg/mL", sep = " ")

    plot_i = cytok_loop %>%
        select(donor, treatment, all_of(i)) %>%
        melt(id.vars = c("donor", "treatment")) %>%
        ggplot(aes(x = treatment, y = value, fill = treatment)) +
        geom_boxplot(color = "black")+
        geom_line(aes(group = donor), linetype = "dotted", color = "black") +
        geom_point(shape = 21, fill = "white", color = "black", size = 2)+
        ggtitle(title) +
        ylab(ylabel)+
        custom_theme

    list_plot_d5[[iteration]] = plot_i
    
    iteration = iteration + 1 
        
    }
    
pdf("~/tmp/cytokines_culture_d5.pdf", width = 5, height= 5)
for(i in 1:length(list_plot_d5)) {
print(list_plot_d5[[i]])
}
dev.off()
